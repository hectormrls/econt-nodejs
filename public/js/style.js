
//funcion para cambiar el color del header al mover la pantalla
function ScrollHeader(){
        var scroll = $(window).scrollTop();
        var header = $(".headercolor");
        var logo = $(".navlogo");

        if (scroll > 200) {
          logo.removeClass('d-none');
          header.css("background" , "#212121");
          header.css("margin" , "0px");
        }
        else{
          logo.addClass('d-none');
          header.css("background" , "transparent"); 
          header.css("margin" , "20px");
        }
        return;
}

//funcion para adaptar tamaño del header cuando se mueve la pantalla
function MovewScroll(){
  var scroll = $(window).scrollTop();
  var d = $(document).height();
  var c = $(this).height();

  var contenido2 = $(".hori").offset();
  contenido2 = contenido2.top;

  if (scroll > 300) {
    scrollPercent = (scroll / (d - c));
    var position = (scrollPercent * ($(document).height()/2.5 - $('.hori').height()));

    if(scroll >= contenido2  ){
      $('.hori').css({'bottom': position });
      $('.hori').css({'margin-bottom': -(position-30)});
    }else{
      $('.hori').css({'bottom': position });
      $('.hori').css({'margin-bottom': -(position-30)});
    }
  }
  else{
      scrollPercent = (scroll / (d - c));
      var position = (scrollPercent * ($(document).height()/2.5 - $('.hori').height()));
      $('.hori').css({'bottom': position });
       }
  return;
}

//función que define las propiedades del carousel de servicios
function SetCarousel(){
  var owl = $('.owl-carousel');
      owl.owlCarousel({
          items:4,
          loop:true,
          margin:10,
          nav:false,
          autoplay:true,
          autoplayTimeout:3000,
          autoplayHoverPause:true,
          responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
      });

      return;
}

//funciones pagina general


function Notif(n){
  var det = document.getElementById("det");
  var badge;
  var vdet = document.getElementById("detalles");
  vdet.classList.remove('d-none');
 switch(n){
     case 1:
     det.innerText = "La Declaracion de Renta ha sido aprobada por el contador, proceda a realizar el pago en la sección 'Mis Pedidos'.";
     badge = document.getElementById("bdg1").innerText = "";
     break;
     case 2:
     det.innerText = "El contador ha empezado a trabajar en sus Estados Financieros, se le notificará una vez terminados.";
     badge = document.getElementById("bdg2").innerText = "";
     break;
     case 3:
     det.innerText = "Sus Registros Contables estan listos, puede descargarlos desde la sección 'Gestión de Documentos'";
     badge = document.getElementById("bdg3").innerText = "";
     break;
 }
 
}

function Pedid(n){
  var nuped = document.getElementById("nuped");
  var faped = document.getElementById("faped");
  var moped = document.getElementById("moped");
  var nstped = document.getElementById("nstped");
  var nomped = document.getElementById("nomped");
  var ncont = document.getElementById("ncont");
  var btnpago = document.getElementById("btnpago");
  var vdet = document.getElementById("detalles");

  vdet.classList.remove('d-none');

 switch(n){
     case 1:
     nuped.innerText = "13456";
     faped.innerText = "En espera";
     moped.innerText = "100.00";
     nstped.innerText = "No Pagado";
     nomped.innerText = "Declaración de Renta";
     ncont.innerText = "Julian Camargo";
     btnpago.classList.remove('d-none');
    
     break;
     case 2:
        nuped.innerText = "53423";
        faped.innerText = "Iniciado";
        moped.innerText = "100.00";
        nstped.innerText = "Pagado";
        nomped.innerText = "Estados Financieros";
        ncont.innerText = "Alexander Gonzalez";
        btnpago.classList.add('d-none');
  
     break;
     case 3:
        nuped.innerText = "86742";
        faped.innerText = "Terminado";
        moped.innerText = "150.00";
        nstped.innerText = "Pagado";
        nomped.innerText = "Registros Contables";
        ncont.innerText = "Anna María Torres";
        btnpago.classList.add('d-none');
     
     break;
 }
 
}

function VerCont(){
var det = document.getElementById("detalles");
det.classList.remove('d-none');
}

function InfCont(x){
  var nom = document.getElementById("nom");
  var ape = document.getElementById("ape");
  var tel = document.getElementById("tel");
  var cor = document.getElementById("cor");
  var idon = document.getElementById("idon");
  var esp = document.getElementById("esp");

  var continf = document.getElementById("continf");
  continf.classList.remove('d-none');

  switch(x){
    case 1:
        nom.innerText = "Julian";
        ape.innerText = "Camargo";
        tel.innerText = "5462-7644";
        cor.innerText = "julian_camargo@hotmail.com";
        idon.innerText = "23-6534-2312";
        esp.innerText = "Estados financieros | Declaración municipal | Declaración de renta | Servicio de nómina";

      break
    case 2:
        nom.innerText = "Alexander";
        ape.innerText = "Gonzalez";
        tel.innerText = "6342-8954";
        cor.innerText = "alex.gnzlez@gmail.com";
        idon.innerText = "45-8675-3421";
        esp.innerText = "ITBMS | Conciliación bancaria | Informes especiales";

      break
    case 3:
        nom.innerText = "Anna María";
        ape.innerText = "Torres";
        tel.innerText = "6734-9054";
        cor.innerText = "ann.mary@live.com";
        idon.innerText = "23-7895-3419";
        esp.innerText = "Servicio de nómina | ITBMS | Registros contables | Declaración municipal";

      break
  }

}



function Msg(){
  alert("Este botón hace algo");
}

function ToggleView(x){
var form1 = document.getElementById("form1");
var form2 = document.getElementById("form2");

  switch (x){
        case 1:
          form1.classList.add('d-none');
          form2.classList.remove('d-none');
        break;
        case 2:
          form1.classList.remove('d-none');
          form2.classList.add('d-none');
        break;
        default:
          form1.classList.remove('d-none');
          form2.classList.add('d-none');
  }
}

function ToggleViewReg(x){
  var form1 = document.getElementById("reg_form1");
  var form2 = document.getElementById("reg_form2");
  var form3 = document.getElementById("reg_form3");

  switch (x){
        case 1:
            form1.classList.remove('d-none');
            form2.classList.add('d-none');
            form3.classList.add('d-none');
        break;
        case 2:
            form1.classList.add('d-none');
            form2.classList.remove('d-none');
            form3.classList.add('d-none');
        break;
        case 3:
            form1.classList.add('d-none');
            form2.classList.add('d-none');
            form3.classList.remove('d-none');
        break;
        default:
            form1.classList.remove('d-none');
            form2.classList.add('d-none');
            form3.classList.add('d-none');
  }
}

function ActualizarInfo(x){
  var btnedit = document.getElementById("btnedit");
  var btnupdate = document.getElementById("btnupdate");
  var btndelete = document.getElementById("btndelete");
  var contra2 = document.getElementById("contra2");
  var nom = document.getElementById("nom");
  var ape = document.getElementById("ape");
  var ced = document.getElementById("ced");
  var tel = document.getElementById("tel");
  var ema = document.getElementById("ema");
  var cont = document.getElementById("cont");
  var cont2 = document.getElementById("cont2");
  switch (x){
      case 0:
      btnedit.classList.remove('d-none');
      btnupdate.classList.add('d-none');
      btndelete.classList.remove('d-none');
      contra2.classList.add('d-none');
      nom.contentEditable = "false";
      ape.contentEditable = "false";
      ced.contentEditable = "false";
      tel.contentEditable = "false";
      ema.contentEditable = "false";
      cont.contentEditable = "false";
      cont2.contentEditable = "false";

      nom.classList.remove('form-control');
      ape.classList.remove('form-control');
      ced.classList.remove('form-control');
      tel.classList.remove('form-control');
      ema.classList.remove('form-control');
      cont.classList.remove('form-control');
      cont2.classList.remove('form-control');

      break;
      case 1:
      btnedit.classList.add('d-none');
      btnupdate.classList.remove('d-none');
      btndelete.classList.add('d-none');
      contra2.classList.remove('d-none');
      nom.contentEditable = "true";
      ape.contentEditable = "true";
      ced.contentEditable = "true";
      tel.contentEditable = "true";
      ema.contentEditable = "true";
      cont.contentEditable = "true";
      cont2.contentEditable = "true";

      nom.classList.add('form-control');
      ape.classList.add('form-control');
      ced.classList.add('form-control');
      tel.classList.add('form-control');
      ema.classList.add('form-control');
      cont.classList.add('form-control');
      cont2.classList.add('form-control');

      break;
  }
}


//Funciones area de administración

function DetVista(x){
  var serv = document.getElementById("detserv");
  var nserv = document.getElementById("addserv");

  switch(x){
      case 1:
      serv.classList.remove('d-none');
      nserv.classList.add('d-none');
          break;
      case 2:
      serv.classList.add('d-none');
      nserv.classList.remove('d-none');
          break;
      case 3:
      serv.classList.add('d-none');
      nserv.classList.add('d-none');
          break;
  }
  
}

function ToggleViewUser(x){
var vcliente = document.getElementById("vcliente");
var vcontador = document.getElementById("vcontador");
var vempresa = document.getElementById("vempresa");

switch (x){
  case 1:
      vcliente.classList.remove('d-none');
      vcontador.classList.add('d-none');
      vempresa.classList.add('d-none');
  break;
  case 2:
      vcliente.classList.add('d-none');
      vcontador.classList.remove('d-none');
      vempresa.classList.add('d-none');
  break;
  case 3:
      vcliente.classList.add('d-none');
      vcontador.classList.add('d-none');
      vempresa.classList.remove('d-none');
  break;
}
}

function ActualizarServ(x){
var serv = document.getElementById("serv");
var pre = document.getElementById("pre");
var doc = document.getElementById("doc");
var desc = document.getElementById("desc");
var btnedit = document.getElementById("btnedit");
var btnupd = document.getElementById("btnupd");
var btndelt = document.getElementById("btndelt");

switch(x){
  case 0:
  serv.contentEditable = "false";
  pre.contentEditable = "false";
  doc.contentEditable = "false";
  desc.contentEditable = "false";

  serv.classList.remove('form-control');
  pre.classList.remove('form-control');
  doc.classList.remove('form-control');
  desc.classList.remove('form-control');

  btnedit.classList.remove('d-none');
  btnupd.classList.add('d-none');
  btndelt.classList.remove('d-none');

      break;
  case 1:
  serv.contentEditable = "true";
  pre.contentEditable = "true";
  doc.contentEditable = "true";
  desc.contentEditable = "true";

  serv.classList.add('form-control');
  pre.classList.add('form-control');
  doc.classList.add('form-control');
  desc.classList.add('form-control');

  btnedit.classList.add('d-none');
  btnupd.classList.remove('d-none');
  btndelt.classList.add('d-none');
      break;
}
}



