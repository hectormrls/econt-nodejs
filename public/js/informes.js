//Datos y configuraciones de las gráficas

var lineal = document.getElementById("grafLinealS");
let grafLinealS = new Chart(lineal, {
    type: 'line',
    data: {
        labels: [,'Julio', 'Agosto', 'Septiembre', 'Octubre'],
        datasets: [{
            label: 'Servicios Contratados',
            data: [0, 7, 19, 23, 30],
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        showLines: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false
                }
            }]
        }
    }
});

var lineal = document.getElementById("grafLinealU");
let grafLinealU = new Chart(lineal, {
    type: 'line',
    data: {
        labels: [,'Julio', 'Agosto', 'Septiembre', 'Octubre'],
        datasets: [{
            label: 'Usuarios Registrados',
            data: [0, 12, 30, 18, 38],
            backgroundColor: [
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        showLines: true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: false
                }
            }]
        }
    }
});

var circular = document.getElementById("grafCircular");
let grafCircular = new Chart(circular, {
    type: 'doughnut',
    data: {
        labels: ['Declaraciones de Renta', 'Conciliaciones Bancarias', 'Estados Financieros', 'Servicios de Nómina', 'Registros Contables',  'ITBMS', 'Informes Especiales'],
        datasets: [{
            label: 'Servicios contratados - Detalle',
            data: [22, 7, 15, 10, 17, 6, 2],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(90, 117, 255, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(90, 117, 255, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }
    ,
    options: {
        showLines: true,
        legend:{
            position: 'bottom'
        }
    }
});

var barra = document.getElementById("grafBarra");
let grafBarra = new Chart(barra, {
    type: 'bar',
    data: {
        labels: ['Julio', 'Agosto', 'Septiembre', 'Octubre'],
        datasets: [{
            label: 'Ingresos en Dolares',
            data: [75, 120, 285, 443],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }
    ,
    options: {
        showLines: true,
        legend:{
            display: false
        }
    }
});