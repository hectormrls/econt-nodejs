//Requirquiendo Firebase
var firebase = require("firebase");
const { Storage } = require('@google-cloud/storage');
var session = require('express-session');

const httpMsgs = require('http-msgs');


// Firebase Config
var firebaseConfig = {
    apiKey: "AIzaSyAh8QRXknMt9SeYJ5XCUQIz1vr9tykAb2g",
    authDomain: "econt-db.firebaseapp.com",
    databaseURL: "https://econt-db.firebaseio.com",
    projectId: "econt-db",
    storageBucket: "econt-db.appspot.com",
    messagingSenderId: "397129186583",
    appId: "1:397129186583:web:67466f1836442f6c"
};

//Google Cloud Bucket Seteo
const gc = new Storage({
    projectId: 'econt-db',
    keyFilename: 'eCont DB-27bca417bfa9.json'
});

const eContBucketFiles = gc.bucket('econt-db.appspot.com');


// function sendUploadToGCS (req, res, next) {
//     if (!req.file) {
//       return next();
//     }
  
//     const gcsname = Date.now() + req.file.originalname;
//     const file = bucket.file(gcsname);
  
//     const stream = file.createWriteStream({
//       metadata: {
//         contentType: req.file.mimetype
//       }
//     });
  
//     stream.on('error', (err) => {
//       req.file.cloudStorageError = err;
//       next(err);
//     });
  
//     stream.on('finish', () => {
//       req.file.cloudStorageObject = gcsname;
//       file.makePublic().then(() => {
//         req.file.cloudStoragePublicUrl = getPublicUrl(gcsname);
//         next();
//       });
//     });
  
//     stream.end(req.file.buffer);
//   }

//   function getPublicUrl (filename) {
//     return `https://storage.googleapis.com/${CLOUD_BUCKET}/${filename}`;
//   }
   // Initialize Firebase
firebase.initializeApp(firebaseConfig);
// var storageRef = firebase.storage().ref();
var db = firebase.firestore();
var express = require('express'),
    ejs = require('ejs');
var helmet = require ('helmet');
var app = express();

app.use(express.urlencoded({
    extended: true
  }));

app.use(session({
    secret: 'k12121aksjaksak',
    resave: true,
    saveUninitialized: true
}));

app.use(helmet());
//Hay que indicarle a node que view engine se va a usar
app.set('view engine', 'ejs');

//Hay que indicarle a node donde estarán los acrhivos públicos
app.use(express.static('public'));

app.get('/', function(req, res){
    console.log("¡Bienvenido! "+req.session.nombreCliente);
    res.locals.title = "eCont";
    res.type('text/html');
    res.render('index', {
        pagina:1,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
    res.setHeader('Content-Security-Policy', "script-src 'self'; style-src 'self'");
    res.setHeader('Strict-Transport-Security: max-age=31536000; includeSubDomains')
});

app.get('/cuenta', function(req, res){
    res.locals.title = "eCont - Mi Cuenta";
    res.type('text/html');
    res.render('index', {
        pagina:2,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente,
        cedula: req.session.cedulaCliente,
        telefono: req.session.telefonoCliente,
        email: req.session.emailCliente,
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/sesion', function(req, res){
    res.locals.title = "eCont - Inicio de Sesión";
    res.type('text/html');
    res.render('index', {
        pagina:3,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/registro', function(req, res){
    res.locals.title = "eCont - Registro de usuario";
    res.type('text/html');
    res.render('index', {
        pagina:4,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/notificaciones', function(req, res){
    res.locals.title = "eCont - Notificaciones";
    res.type('text/html');
    res.render('index', {
        pagina:5,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/pedidos', function(req, res){
    res.locals.title = "eCont - Mis Pedidos";
    res.type('text/html');
    res.render('index', {
        pagina:6,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/gestion', function(req, res){
    res.locals.title = "eCont - Gestión de Documentos";
    res.type('text/html');
    res.render('index', {
        pagina:7,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/contacto', function(req, res){
    res.locals.title = "eCont - Contacto";
    res.type('text/html');
    res.render('index', {
        pagina:8,
        nombre: req.session.nombreCliente,
        apellido: req.session.apellidoCliente
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/servicio', function(req, res){
    res.locals.title = "eCont";
    var idServicio = req.query.servid
    idServicio = String(idServicio);
    var colection = db.collection('servicios');
    colection.where('id', '==', idServicio).get().then(resp=>{
        if(!resp.empty){
            var docs = [];
            docs = resp.docs.map(doc => doc.data().docs);
            for(var i=0; i<docs.length; i++){
                console.log(docs[0]);
            }
            res.type('text/html');
            res.render('index', {
                pagina:9,
                desc: resp.docs.map(doc => doc.data().desc),
                image: resp.docs.map(doc => doc.data().image),
                nombreS: resp.docs.map(doc => doc.data().nombre),
                docs : docs,
                id: idServicio,
                nombre: req.session.nombreCliente,
                apellido: req.session.apellidoCliente
            }, function(err, html){
                if(err) throw err;
                res.send(html);
            });
        }
        else{
            console.log("Algo ha salido mal");
        }
    });
});

app.get('/cerrar', function(req,res){
    res.locals.title = "eCont";
    req.session.destroy();
    res.render('index', {
        pagina:1,
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
})

app.get('/documentos', function(req, res){
    res.locals.title = "eCont";
    var idServicio = req.query.servid
    idServicio = String(idServicio);
    var colection = db.collection('servicios');
    colection.where('id', '==', idServicio).get().then(resp=>{
        if(!resp.empty){
            var docs = [];
            docs = resp.docs.map(doc => doc.data().docs);
            for(var i=0; i<docs.length; i++){
                console.log(docs[0]);
            }
            res.type('text/html');
            res.render('index', {
                pagina:10,
                desc: resp.docs.map(doc => doc.data().desc),
                image: resp.docs.map(doc => doc.data().image),
                nombreS: resp.docs.map(doc => doc.data().nombre),
                docs : docs,
                id: idServicio,
                nombre: req.session.nombreCliente,
                apellido: req.session.apellidoCliente
            }, function(err, html){
                if(err) throw err;
                res.send(html);
            });
        }
        else{
            console.log("Algo ha salido mal");
        }
    });
});

//INICIO DE SESIÓN
app.post('/iniciar', function(req, resp){
    resp.locals.title = "eCont - Iniciando Sesion";
    var email =  req.body.email;
    var contra = req.body.contra;
    console.log(email, contra);
    var colection = db.collection('usuarios');
    colection.where('email', '==', email).where('contra', '==', contra).get().then(res=>{
        if(!res.empty){
            console.log(res.docs.map(doc => doc.data().nombre));
            var nom = res.docs.map(doc => doc.data().nombre);
            var ced = res.docs.map(doc => doc.data().cedula);
            var apellido = res.docs.map(doc => doc.data().apellido);
            var email = res.docs.map(doc => doc.data().email);
            var telefono = res.docs.map(doc => doc.data().telefono);

            req.session.nombreCliente = nom;
            req.session.apellidoCliente = apellido;
            req.session.cedulaCliente = ced;
            req.session.emailCliente = email;
            req.session.telefonoCliente = telefono;
            resp.status(200).json({nombre: nom, cedula:ced, apellido: apellido, email:email, telefono:telefono});
        }
        else{
            resp.status(500).send();
        }
    });
    // res.type('text/html');
    // res.render('index', {
    //     pagina:11
    // }, function(err, html){
    //     if(err) throw err;
    //     res.send(html);
});

app.post('/iniciaradmin', function(req, resp){
    resp.locals.title = "eCont - Iniciando Sesion";
    var email =  req.body.email;
    var contra = req.body.contra;
    var colection = db.collection('usuarios');
    colection.where('email', '==', email).where('contra', '==', contra).where('tipo', '==', 'adm').get().then(res=>{
        if(!res.empty){
            var nom = res.docs.map(doc => doc.data().nombre);
            var ced = res.docs.map(doc => doc.data().cedula);
            var apellido = res.docs.map(doc => doc.data().apellido);
            var email = res.docs.map(doc => doc.data().email);
            var telefono = res.docs.map(doc => doc.data().telefono);

            req.session.nombreCliente = nom;
            req.session.apellidoCliente = apellido;
            req.session.cedulaCliente = ced;
            req.session.emailCliente = email;
            req.session.telefonoCliente = telefono;
            resp.status(200).json({nombre: nom, cedula:ced, apellido: apellido, email:email, telefono:telefono});
        }
        else{
            resp.status(500).send();
        }
    });
    // res.type('text/html');
    // res.render('index', {
    //     pagina:11
    // }, function(err, html){
    //     if(err) throw err;
    //     res.send(html);
});


//a partir de aquí son las pantallas de Administración
app.get('/admin', function(req, res){
    res.locals.title = "eCont - Administración";
    res.type('text/html');
    res.render('administracion', {
        pagina:1
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/registroexitoso', function(req, res){
    res.locals.title = "eCont - Registrado Correctamente";
    res.type('text/html');
    res.render('index', {
        pagina:11
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/informes', function(req, res){
    res.locals.title = "eCont - Informes";
    res.type('text/html');
    res.render('administracion', {
        pagina:2
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/clientes', function(req, res){
    res.locals.title = "eCont - Clientes";
    res.type('text/html');
    res.render('administracion', {
        pagina:3
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});
    
app.get('/servicios', function(req, res){
    res.locals.title = "eCont - Servicios";
    res.type('text/html');
    res.render('administracion', {
        pagina:4
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});

app.get('/solicitudes', function(req, res){
    res.locals.title = "eCont - Servicios";
    var array = [];
    var colection = db.collection('usuarios');
    colection.where('status', '==', '0').get().then(snap=>{
        var array = [];
        snap.forEach(doc=>{
            array = doc.data().servicios
            console.log(array);
        });
    });
    res.type('text/html');
    res.render('administracion', {
        pagina:5
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });
});


//MÉTODOS POST
app.post('/sendfb', function(req, res){
    res.locals.title = "eCont - Registro";
    console.log()
    var colection = db.collection('usuarios');
    colection.where('cedula', '==', req.body.cedula).get().then(resp=>{
        if(!resp.empty){
            res.status(500).send("Esta cédula ya se encuentra registrada. Porfavor inicie sesión.")
        }
        else{
            colection.where('email', '==', req.body.email).get().then(resp2=>{
                if(!resp2.empty){
                    res.status(500).send("Este correo electrónico ya se encuentra registrado. Porfavor cámbielo o Inicie sesión.")
                }
                else{
                    db.collection("usuarios").add({
                                    nombre: req.body.nombre,
                                    apellido: req.body.apellido,
                                    cedula: req.body.cedula,
                                    telefono: req.body.telefono,
                                    email: req.body.email,
                                    contra:req.body.contra,
                                    tipo: req.body.tipo
                    }).then(resp3=>{
                         console.log(resp3);
                    });
                    res.redirect("/registroexitoso");
                //FIN SEGUNDO ELSE - (CORREO)
                } 
            });
        //FIN PRIMER ELSE - (CEDULA)
        }
    });
});

app.post('/sendfbc', function(req, res){
    res.locals.title = "eCont - Registro";
    console.log()
    var colection = db.collection('usuarios');
    colection.where('cedula', '==', req.body.cedula).get().then(resp=>{
        if(!resp.empty){
            res.status(500).send("Esta cédula ya se encuentra registrada. Porfavor inicie sesión.")
        }
        else{
            colection.where('email', '==', req.body.email).get().then(resp2=>{
                if(!resp2.empty){
                    res.status(500).send("Este correo electrónico ya se encuentra registrado. Porfavor cámbielo o Inicie sesión.")
                }
                else{
                    db.collection("usuarios").add({
                                    nombre: req.body.nombre,
                                    apellido: req.body.apellido,
                                    cedula: req.body.cedula,
                                    telefono: req.body.telefono,
                                    email: req.body.email,
                                    contra:req.body.contra,
                                    tipo: req.body.tipo,
                                    status: req.body.status,
                                    idoneidad: req.body.idoneidad,
                                    servicios: req.body.servicios
                    }).then(resp3=>{
                         console.log(resp3);
                    });
                    res.redirect("/registroexitoso");
                //FIN SEGUNDO ELSE - (CORREO)
                } 
            });
        //FIN PRIMER ELSE - (CEDULA)
        }
    });
});

app.post('/sendfbe', function(req, res){
    res.locals.title = "eCont - Registro";
    console.log()
    var colection = db.collection('usuarios');
    colection.where('cedula', '==', req.body.cedula).get().then(resp=>{
        if(!resp.empty){
            res.status(500).send("Esta cédula ya se encuentra registrada. Porfavor inicie sesión.")
        }
        else{
            colection.where('email', '==', req.body.email).get().then(resp2=>{
                if(!resp2.empty){
                    res.status(500).send("Este correo electrónico ya se encuentra registrado. Porfavor cámbielo o Inicie sesión.")
                }
                else{
                    db.collection("usuarios").add({
                                    nombre: req.body.nombre,
                                    apellido: req.body.apellido,
                                    cedula: req.body.cedula,
                                    telefono: req.body.telefono,
                                    email: req.body.email,
                                    contra:req.body.contra,
                                    tipo: req.body.tipo,
                                    status: req.body.status,
                                    empresa: req.body.empresa,
                                    ruc: req.body.ruc
                    }).then(resp3=>{
                         console.log(resp3);
                    });
                    res.redirect("/registroexitoso");
                //FIN SEGUNDO ELSE - (CORREO)
                } 
            });
        //FIN PRIMER ELSE - (CEDULA)
        }
    });
});
// app.post('/registrocontador',multer.single('cv'), sendUploadToGCS,  (req,res,next)=>{
//     if (req.file && req.file.cloudStoragePublicUrl) {
//         data.imageUrl = req.file.cloudStoragePublicUrl;
//       }
// });

//Error 404
app.use(function(req, res){
    res.locals.title = "eCont - Error";
    res.type('text/html');
    res.status(404);
    res.render('index', {
        pagina:40
    }, function(err, html){
        if(err) throw err;
        res.send(html);
    });

});

// Pagina de error 500
app.use(function(err, req, res, next){
    res.locals.title = "eCont - Error";
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Server Error');
});

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), function(){
    console.log( 'Servidor iniciado en http://localhost:' +
    app.get('port') + '; presiona Ctrl-C para terminar.' );
});
